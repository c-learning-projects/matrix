#include <check.h>
#include "../s21_matrix.h"

START_TEST(create_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    int res = 1;
    for (int i = 0; i < A.rows && res == 1; i++) {
        for (int j = 0; j < A.columns && res == 1; j++) {
            if (A.matrix[i][j] != 0.0) res = 0;
        }
    }
    ck_assert_int_eq(res, 1);
    ck_assert_int_eq(A.matrix_type, ZERO_MATRIX);
    s21_remove_matrix(&A);
}
END_TEST

START_TEST(remove_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    s21_remove_matrix(&A);
    ck_assert_int_eq(A.columns, 0);
    ck_assert_int_eq(A.rows, 0);
    ck_assert_int_eq(A.matrix_type, INCORRECT_MATRIX);
}
END_TEST

START_TEST(eq_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t B = s21_create_matrix(2, 2);
    ck_assert_int_eq(0, s21_eq_matrix(&A, &B));
    s21_remove_matrix(&A);
    s21_remove_matrix(&B);
}
END_TEST

START_TEST(eq_matrix_fail) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t B = s21_create_matrix(3, 3);
    ck_assert_int_eq(1, s21_eq_matrix(&A, &B));
    s21_remove_matrix(&A);
    s21_remove_matrix(&B);
}
END_TEST

START_TEST(sum_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t B = s21_create_matrix(3, 3);
    matrix_t C = s21_sum_matrix(&A, &B);
    ck_assert_int_eq(1, s21_eq_matrix(&A, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&B);
}
END_TEST

START_TEST(sub_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t B = s21_create_matrix(3, 3);
    matrix_t C = s21_sub_matrix(&A, &B);
    ck_assert_int_eq(1, s21_eq_matrix(&A, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&B);
}
END_TEST

START_TEST(mult_matrix) {
    matrix_t A = s21_create_matrix(3, 2);
    matrix_t B = s21_create_matrix(2, 3);
    matrix_t check = s21_create_matrix(3, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 4;
    A.matrix[1][0] = 2; A.matrix[1][1] = 5;
    A.matrix[2][0] = 3; A.matrix[2][1] = 6;
    B.matrix[0][0] = 1; B.matrix[0][1] = -1; B.matrix[0][2] = 1;
    B.matrix[1][0] = 2; B.matrix[1][1] = 3; B.matrix[1][2] = 4;
    check.matrix[0][0] = 9; check.matrix[0][1] = 11; check.matrix[0][2] = 17;
    check.matrix[1][0] = 12; check.matrix[1][1] = 13; check.matrix[1][2] = 22;
    check.matrix[2][0] = 15; check.matrix[2][1] = 15; check.matrix[2][2] = 27;
    matrix_t C = s21_mult_matrix(&A, &B);
    ck_assert_int_eq(1, s21_eq_matrix(&check, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&B);
    s21_remove_matrix(&C);
    s21_remove_matrix(&check);
}
END_TEST

START_TEST(mult_number) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t C = s21_mult_number(&A, 1);
    ck_assert_int_eq(1, s21_eq_matrix(&A, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&C);
}
END_TEST

START_TEST(transpose) {
    matrix_t A = s21_create_matrix(3, 2);
    matrix_t check = s21_create_matrix(2, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 4;
    A.matrix[1][0] = 2; A.matrix[1][1] = 5;
    A.matrix[2][0] = 3; A.matrix[2][1] = 6;
    check.matrix[0][0] = 1; check.matrix[0][1] = 2; check.matrix[0][2] = 3;
    check.matrix[1][0] = 4; check.matrix[1][1] = 5; check.matrix[1][2] = 6;
    matrix_t C = s21_transpose(&A);
    ck_assert_int_eq(1, s21_eq_matrix(&check, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&C);
    s21_remove_matrix(&check);
}
END_TEST

START_TEST(determinant) {
    matrix_t A = s21_create_matrix(3, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 2; A.matrix[0][2] = 3;
    A.matrix[1][0] = 4; A.matrix[1][1] = 5; A.matrix[1][2] = 6;
    A.matrix[2][0] = 7; A.matrix[2][1] = 8; A.matrix[2][2] = 9;
    ck_assert_int_eq(0, s21_determinant(&A));
    s21_remove_matrix(&A);
}
END_TEST

START_TEST(determinant_fail) {
    matrix_t A = s21_create_matrix(2, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 2; A.matrix[0][2] = 3;
    A.matrix[1][0] = 4; A.matrix[1][1] = 5; A.matrix[1][2] = 6;
    ck_assert_int_eq(1, s21_isnan(s21_determinant(&A)));
    s21_remove_matrix(&A);
}
END_TEST

START_TEST(determinant_1) {
    matrix_t A = s21_create_matrix(1, 1);
    A.matrix[0][0] = 10;
    ck_assert_int_eq(10, s21_determinant(&A));
    s21_remove_matrix(&A);
}
END_TEST

START_TEST(calc_complements) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t check = s21_create_matrix(3, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 2; A.matrix[0][2] = 3;
    A.matrix[1][0] = 0; A.matrix[1][1] = 4; A.matrix[1][2] = 2;
    A.matrix[2][0] = 5; A.matrix[2][1] = 2; A.matrix[2][2] = 1;
    check.matrix[0][0] = 0; check.matrix[0][1] = 10; check.matrix[0][2] = -20;
    check.matrix[1][0] = 4; check.matrix[1][1] = -14; check.matrix[1][2] = 8;
    check.matrix[2][0] = -8; check.matrix[2][1] = -2; check.matrix[2][2] = 4;
    matrix_t C = s21_calc_complements(&A);
    ck_assert_int_eq(1, s21_eq_matrix(&check, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&C);
    s21_remove_matrix(&check);
}
END_TEST

START_TEST(calc_complements_fail) {
    matrix_t A = s21_create_matrix(2, 3);
    A.matrix[0][0] = 1; A.matrix[0][1] = 2; A.matrix[0][2] = 3;
    A.matrix[1][0] = 0; A.matrix[1][1] = 4; A.matrix[1][2] = 2;
    matrix_t C = s21_calc_complements(&A);
    ck_assert_int_eq(INCORRECT_MATRIX, C.matrix_type);
    s21_remove_matrix(&A);
    s21_remove_matrix(&C);
}
END_TEST

START_TEST(inverse_matrix) {
    matrix_t A = s21_create_matrix(3, 3);
    matrix_t check = s21_create_matrix(3, 3);
    A.matrix[0][0] = 2; A.matrix[0][1] = 5; A.matrix[0][2] = 7;
    A.matrix[1][0] = 6; A.matrix[1][1] = 3; A.matrix[1][2] = 4;
    A.matrix[2][0] = 5; A.matrix[2][1] = -2; A.matrix[2][2] = -3;
    check.matrix[0][0] = 1; check.matrix[0][1] = -1; check.matrix[0][2] = 1;
    check.matrix[1][0] = -38; check.matrix[1][1] = 41; check.matrix[1][2] = -34;
    check.matrix[2][0] = 27; check.matrix[2][1] = -29; check.matrix[2][2] = 24;
    matrix_t C = s21_inverse_matrix(&A);
    ck_assert_int_eq(1, s21_eq_matrix(&check, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&check);
}
END_TEST

START_TEST(inverse_matrix_1) {
    matrix_t A = s21_create_matrix(1, 1);
    matrix_t check = s21_create_matrix(1, 1);
    A.matrix[0][0] = 2;
    check.matrix[0][0] = 1.0 / 2.0;
    matrix_t C = s21_inverse_matrix(&A);
    ck_assert_int_eq(1, s21_eq_matrix(&check, &C));
    s21_remove_matrix(&A);
    s21_remove_matrix(&check);
}
END_TEST

START_TEST(inverse_matrix_fail) {
    matrix_t A = s21_create_matrix(2, 3);
    matrix_t check = s21_create_matrix(3, 3);
    A.matrix[0][0] = 2; A.matrix[0][1] = 5; A.matrix[0][2] = 7;
    A.matrix[1][0] = 6; A.matrix[1][1] = 3; A.matrix[1][2] = 4;
    matrix_t C = s21_inverse_matrix(&A);
    ck_assert_int_eq(INCORRECT_MATRIX, C.matrix_type);
    s21_remove_matrix(&A);
    s21_remove_matrix(&C);
    s21_remove_matrix(&check);
}
END_TEST

Suite *new_suite_create(void) {
    Suite *suite = suite_create("Core");
    TCase *tcase_core = tcase_create("Core of new");
    tcase_add_test(tcase_core, create_matrix);
    tcase_add_test(tcase_core, remove_matrix);
    tcase_add_test(tcase_core, eq_matrix);
    tcase_add_test(tcase_core, sum_matrix);
    tcase_add_test(tcase_core, sub_matrix);
    tcase_add_test(tcase_core, mult_number);
    tcase_add_test(tcase_core, mult_matrix);
    tcase_add_test(tcase_core, transpose);
    tcase_add_test(tcase_core, calc_complements);
    tcase_add_test(tcase_core, determinant);
    tcase_add_test(tcase_core, inverse_matrix);
    tcase_add_test(tcase_core, eq_matrix_fail);
    tcase_add_test(tcase_core, calc_complements_fail);
    tcase_add_test(tcase_core, determinant_fail);
    tcase_add_test(tcase_core, determinant_1);
    tcase_add_test(tcase_core, inverse_matrix_1);
    tcase_add_test(tcase_core, inverse_matrix_fail);
    suite_add_tcase(suite, tcase_core);
    return suite;
}

int main(void) {
    Suite *suite = new_suite_create();
    SRunner *suite_runner = srunner_create(suite);
    srunner_run_all(suite_runner, CK_NORMAL);
    int failed_count = srunner_ntests_failed(suite_runner);
    srunner_free(suite_runner);
    return failed_count;
}
